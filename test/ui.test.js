const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await page.waitFor(500);
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(title).toBe('Join');
    await page.waitFor(500);
    await browser.close();
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await page.waitFor(500);
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 800}); 
    await page.screenshot({path: 'test/screenshots/welcomMessage.png'});
    await page.waitFor(500);
    await browser.close();

})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.keyboard.press('Enter', {delay: 800}); 
    await page.screenshot({path: 'test/screenshots/errorMessage.png'});
    await page.waitFor(500);
    await browser.close();

})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitFor(1000);    
    let member = await page.evaluate(() => {
        return document.querySelector('#messages > li > div:nth-child(2) > p').innerHTML;
    });

    expect(member).toBe('Hi John, Welcome to the chat app');
    
    await page.waitFor(500);
    await browser.close();

})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await page.waitFor(500);
    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(userName).toBe('R1');

    await page.waitFor(500);
    await browser.close();

})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await page.waitFor(500);
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page1 = await browser.newPage();
    await page1.goto(url);
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100}); 

    await page1.waitFor(1000);    

    const page2 = await browser.newPage();
    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page2.waitFor(1000);    

    // test page
    let user1 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    let user2 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(user1).toBe('John');
    expect(user2).toBe('Mike');


    await page1.waitFor(500);
    await browser.close();

})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    let button = await page.$eval('#message-form > button', (content) => content.innerHTML);
    expect(button).toBe('Send');

    
    await page.waitFor(500);
    await browser.close();

})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitFor(1000);    
    await page.type('#message-form > input[type=text]', 'abc', {delay:100});
    await page.keyboard.press('Enter', {delay: 500}); 
    let message = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });
    expect(message).toBe('abc');

    await page.waitFor(500);
    await browser.close();

})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page1 = await browser.newPage();
    await page1.goto(url);
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100}); 
    await page1.waitFor(1000);

    const page2 = await browser.newPage();
    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 
    await page2.waitFor(1000);

    await page1.type('#message-form > input[type=text]', 'Hi', {delay:100});
    await page1.keyboard.press('Enter', {delay: 100}); 

    await page2.type('#message-form > input[type=text]', 'Hello', {delay:100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    let message1 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(2) > p').innerHTML;
    });
    let message2 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(2) > p').innerHTML;
    });
    expect(message1).toBe('Hi'); // user1 say Hi
    expect(message2).toBe('Hello');// user2 say Hello
  
    // 
    await page1.waitFor(1000);
    let user1 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    let user2 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(user1).toBe('John'); //user1 is John
    expect(user2).toBe('Mike'); //user2 is Mike
    await page1.waitFor(500);
    await browser.close();
    
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    let button = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(button).toBe('Send location');
    
    await page.waitFor(500);
    await browser.close();


})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    //
    // to do...
    //
    await page.waitFor(500);
    await browser.close();

})
